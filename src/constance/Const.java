package constance;

import model.Error;

public class Const {
    public static final Error INVALID_NAME = new Error("Your name is invalid","NAME_INVALID");
    public static final Error INVALID_TYPE = new Error("Victim's type is invalid","TYPE_INVALID");
    public static final Error INVALID_STATUS = new Error("Victim's status is invalid","STATUS_INVALID");
    public static final Error CANNOT_CREATED = new Error("Can not create victim","CREATED_FAIL");
}
