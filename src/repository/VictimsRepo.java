package repository;

import model.Victims;
import config.DB;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class VictimsRepo {

    private static final String QUERY_CREATE_VICTIMS = "insert into covid.victims (name, type,phone, status, number_address, " +
            "ward_address, district_address, city_address, lat , _long, age, inflected_time) " +
            "values(?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String QUERY_FILTER_BY_NAME = "select * from covid.victims where name like '%?%' Limit ?,?";
    private static final String QUERY_GET_ALL = "select * from covid.victims";

    public Victims addVictims(Victims newVic) {
        DB db = new DB();
        try {
            Connection connection = db.connectDB();
            PreparedStatement preparedStatement = connection.prepareStatement(QUERY_CREATE_VICTIMS);
            preparedStatement.setString(1, newVic.getName());
            preparedStatement.setString(2, newVic.getType());
            preparedStatement.setString(3, newVic.getPhone());
            preparedStatement.setString(4, newVic.getStatus());
            preparedStatement.setString(5, newVic.getNumber_address());
            preparedStatement.setString(6, newVic.getWard_address());
            preparedStatement.setString(7, newVic.getDistrict_address());
            preparedStatement.setString(8, newVic.getCity_address());
            preparedStatement.setDouble(9, newVic.getLat());
            preparedStatement.setDouble(10, newVic.get_long());
            preparedStatement.setFloat(11, newVic.getAge());
            preparedStatement.setDate(12,new java.sql.Date(newVic.getInflected_time().getTime()));
            if (preparedStatement.execute()){
                return newVic;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<Victims> getAll() {
        DB db = new DB();
        List<Victims> list = new ArrayList<Victims>();
        try{
            Connection connection = db.connectDB();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(QUERY_GET_ALL);
            while (rs.next()) {
                Victims victims = new Victims();
                victims.setId(rs.getInt("id"));
                victims.setName(rs.getString("name"));
                victims.setAge(rs.getInt("age"));
                victims.setPhone(rs.getString("phone"));
                victims.setStatus(rs.getString("status"));
                victims.setType(rs.getString("type"));
                victims.set_long(rs.getDouble("_long"));
                victims.setNumber_address(rs.getString("number_address"));
                victims.setCity_address(rs.getString("city_address"));
                victims.setDistrict_address(rs.getString("district_address"));
                victims.setLat(rs.getDouble("lat"));
                victims.setWard_address(rs.getString("ward_address"));
                victims.setInflected_time(rs.getDate("inflected_time"));
                list.add(victims);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }
}
