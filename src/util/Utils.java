package util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Utils {
    public static String convertDateToStringYYMMDD(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }
}
