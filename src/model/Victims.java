package model;

import java.util.Date;

public class Victims {
    private String name;
    private int id;
    private String type;
    private String phone;
    private String status;
    private String number_address;
    private String ward_address;
    private String district_address;
    private String city_address;
    private double lat;
    private double _long;
    private int age;
    private Date inflected_time;

    public Victims(){}

    public Victims(String name, String type, String phone, String status, String number_address, String ward_address,
                   String district_address, String city_address, double lat, double _long, int age) {
        this.name = name;
        this.type = type;
        this.phone = phone;
        this.status = status;
        this.number_address = number_address;
        this.ward_address = ward_address;
        this.district_address = district_address;
        this.city_address = city_address;
        this.lat = lat;
        this._long = _long;
        this.age = age;
    }


    public Date getInflected_time() {
        return inflected_time;
    }

    public void setInflected_time(Date inflected_time) {
        this.inflected_time = inflected_time;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumber_address() {
        return number_address;
    }

    public void setNumber_address(String number_address) {
        this.number_address = number_address;
    }

    public String getWard_address() {
        return ward_address;
    }

    public void setWard_address(String ward_address) {
        this.ward_address = ward_address;
    }

    public String getDistrict_address() {
        return district_address;
    }

    public void setDistrict_address(String district_address) {
        this.district_address = district_address;
    }

    public String getCity_address() {
        return city_address;
    }

    public void setCity_address(String city_address) {
        this.city_address = city_address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double get_long() {
        return _long;
    }

    public void set_long(double _long) {
        this._long = _long;
    }
}
