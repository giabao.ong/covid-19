package client;

import static com.teamdev.jxbrowser.engine.RenderingMode.HARDWARE_ACCELERATED;

import com.teamdev.jxbrowser.browser.Browser;
import com.teamdev.jxbrowser.engine.Engine;
import com.teamdev.jxbrowser.engine.EngineOptions;
import com.teamdev.jxbrowser.view.swing.BrowserView;

import javax.swing.*;
import java.awt.*;

public class MapPanel extends JPanel {
    private EngineOptions options;
    private Engine engine;
    private Browser browser;
    private BrowserView view;

    public EngineOptions getOptions() {
        return options;
    }

    public void setOptions(EngineOptions options) {
        this.options = options;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Browser getBrowser() {
        return browser;
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public BrowserView getView() {
        return view;
    }

    public void setView(BrowserView view) {
        this.view = view;
    }

    public MapPanel(){
        setLayout(new BorderLayout());
        // Creating and running Chromium engine
        options = EngineOptions.newBuilder(HARDWARE_ACCELERATED).licenseKey("1BNDHFSC1FVQ2HHF1AWXMZQVUCSDQWHVAS90NZOVBKDMMC0VJ0SLVO7Q8PLEUJTD13RV3B").build();
        engine = Engine.newInstance(options);
        browser = engine.newBrowser();
        view = BrowserView.newInstance(browser);
        browser.navigation().loadUrl("E://java//project//covid-19");
        add(view);
    }
}
