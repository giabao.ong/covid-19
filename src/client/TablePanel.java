package client;
import controller.VictimsController;
import model.Victims;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.regex.PatternSyntaxException;


public class TablePanel extends JPanel{
    private TableModel model;
    private JTable table;

    public TableModel getModel() {
        return model;
    }

    public void setModel(TableModel model) {
        this.model = model;
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public TablePanel(){
        setLayout(new BorderLayout());
        VictimsController victimsController = new VictimsController();
        List<Victims> list = victimsController.getAllVictims();
        Object[][] rows = new Object[list.size()][6];
        int i = 0;
        for(Victims v : list){
            rows[i][0] = v.getName();
            rows[i][1] = v.getType();
            rows[i][2] = v.getPhone();
            rows[i][3] = v.getStatus();
            rows[i][4] = v.getNumber_address() +" " + v.getDistrict_address()+" " + v.getCity_address();
            rows[i][5] = v.getAge();
            i++;
        }
        Object columns[] = {"Name", "Type", "Phone","Status","Address","Age"};
        model = new DefaultTableModel(rows, columns){
            public Class getColumnClass(int column){
                Class returnValue;
                if((column >= 0) && (column < getColumnCount())) {
                    returnValue = getValueAt(0, column).getClass();
                } else {
                    returnValue = Object.class;
                }
                return returnValue;
            }
        };

        table = new JTable(model);
        table.setFont(new Font("Verdana",Font.PLAIN, 17));
        table.getTableHeader().setFont(new Font("Verdana",Font.BOLD, 20));
        table.setRowHeight(30);

        final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        add(new JScrollPane(table), BorderLayout.CENTER);

        JPanel panel = new JPanel(new BorderLayout());

        JLabel label = new JLabel("Filter: ");
        label.setFont(new Font("Verdana",Font.PLAIN, 25));
        panel.add(label, BorderLayout.WEST);

        final JTextField filterText = new JTextField("");
        panel.add(filterText, BorderLayout.CENTER);

        add(panel, BorderLayout.NORTH);

        final JButton button = new JButton("Filter");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = filterText.getText();
                if(text.length() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    try {
                        sorter.setRowFilter(RowFilter.regexFilter(text));
                    } catch(PatternSyntaxException pse) {
                        System.out.println("Bad regex pattern");
                    }
                }
            }
        });
        panel.add(button, BorderLayout.EAST);
    }
}
