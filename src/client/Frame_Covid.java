package client;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Frame_Covid extends JFrame{
    private MapPanel mapPanel;
    private TablePanel tablePanel;
    private JPanel panelButtons;
    private JButton buttonTable, buttonMap;

    public Frame_Covid(){
        //Create Table Panel and Map Panel
        mapPanel = new MapPanel();
        tablePanel = new TablePanel();

        //Create buttons to switch between Map and Table
        prepareButtonsPanel();

        //Create Frame and add Buttons, Panels.
        setTitle("Covid-19");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mapPanel.getEngine().close();
            }
        });
        //setLayout(new GridBagLayout());
        setMinimumSize(new Dimension(1000,1000));
        setVisible(true);
        setLocationRelativeTo(null);
        add(panelButtons, BorderLayout.NORTH);
        add(tablePanel);
       /* add(panelButtons, new GridBagConstraints(0, 0, 2, 1,
                1.0, 0.0,
                GridBagConstraints.NORTH, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        panelButtons.setBackground(Color.BLUE);
        tablePanel.setBackground(Color.RED);
        add(tablePanel,new GridBagConstraints(0, 1, 2, 1,
                1.0, 0.0,
                GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
*/
    }


    private void prepareButtonsPanel(){
        buttonMap = new JButton("Map");
        buttonMap.setBounds(0,0,95,30);
        buttonMap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchPanels(mapPanel);
            }
        });

        buttonTable = new JButton("Table");
        buttonTable.setBounds(0,95,95,30);
        buttonTable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switchPanels(tablePanel);
            }
        });

        panelButtons = new JPanel();
        panelButtons.add(buttonMap);
        panelButtons.add(buttonTable);
    }

    private void switchPanels(JPanel panel){
        getContentPane().removeAll();
        /*add(panelButtons, new GridBagConstraints(0, 0, 2, 1,
                1.0, 0.0,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(panel, new GridBagConstraints(0, 1, 2, 1,
                1.0, 0.0,
                GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));*/
        add(panelButtons, BorderLayout.NORTH);
        add(panel);
        repaint();
        revalidate();
    }
}