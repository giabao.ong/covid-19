package controller;

import constance.Const;
import model.Error;
import model.Victims;
import repository.VictimsRepo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class VictimsController {
    public Error addVictim(Victims newVictim) {
        if(newVictim.getName().equals("")){
            return Const.INVALID_NAME;
        } else if (newVictim.getStatus().equals("")){
            return Const.INVALID_STATUS;
        } else if (newVictim.getType().equals("")){
            return Const.INVALID_TYPE;
        }
        Date date = new Date();
        newVictim.setInflected_time(date);
        VictimsRepo repo = new VictimsRepo();
        Victims vic = repo.addVictims(newVictim);
        if (vic != null){
            return null;
        }
        return Const.CANNOT_CREATED;
    }

    public List<Victims> getAllVictims() {
        try {
            VictimsRepo repo = new VictimsRepo();
            List<Victims> list = repo.getAll();
            if ( list.size() > 0 )
                return list;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
